! function (t) {
	var e = {};

	function n(r) {
		if (e[r]) return e[r].exports;
		var o = e[r] = {
			i: r,
			l: !1,
			exports: {}
		};
		return t[r].call(o.exports, o, o.exports, n), o.l = !0, o.exports
	}
	n.m = t, n.c = e, n.d = function (t, e, r) {
		n.o(t, e) || Object.defineProperty(t, e, {
			enumerable: !0,
			get: r
		})
	}, n.r = function (t) {
		"undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
			value: "Module"
		}), Object.defineProperty(t, "__esModule", {
			value: !0
		})
	}, n.t = function (t, e) {
		if (1 & e && (t = n(t)), 8 & e) return t;
		if (4 & e && "object" == typeof t && t && t.__esModule) return t;
		var r = Object.create(null);
		if (n.r(r), Object.defineProperty(r, "default", {
				enumerable: !0,
				value: t
			}), 2 & e && "string" != typeof t)
			for (var o in t) n.d(r, o, function (e) {
				return t[e]
			}.bind(null, o));
		return r
	}, n.n = function (t) {
		var e = t && t.__esModule ? function () {
			return t.default
		} : function () {
			return t
		};
		return n.d(e, "a", e), e
	}, n.o = function (t, e) {
		return Object.prototype.hasOwnProperty.call(t, e)
	}, n.p = "", n(n.s = 48)
}([function (t, e, n) {
	(function (e) {
		var n = function (t) {
			return t && t.Math == Math && t
		};
		t.exports = n("object" == typeof globalThis && globalThis) || n("object" == typeof window && window) || n("object" == typeof self && self) || n("object" == typeof e && e) || Function("return this")()
	}).call(this, n(50))
}, function (t, e) {
	t.exports = function (t) {
		try {
			return !!t()
		} catch (t) {
			return !0
		}
	}
}, function (t, e) {
	t.exports = function (t) {
		return "object" == typeof t ? null !== t : "function" == typeof t
	}
}, function (t, e, n) {
	var r = n(2);
	t.exports = function (t) {
		if (!r(t)) throw TypeError(String(t) + " is not an object");
		return t
	}
}, function (t, e, n) {
	var r = n(0),
		o = n(31),
		i = n(6),
		a = n(32),
		u = n(42),
		c = n(65),
		l = o("wks"),
		f = r.Symbol,
		s = c ? f : f && f.withoutSetter || a;
	t.exports = function (t) {
		return i(l, t) || (u && i(f, t) ? l[t] = f[t] : l[t] = s("Symbol." + t)), l[t]
	}
}, function (t, e, n) {
	var r = n(1);
	t.exports = !r((function () {
		return 7 != Object.defineProperty({}, 1, {
			get: function () {
				return 7
			}
		})[1]
	}))
}, function (t, e) {
	var n = {}.hasOwnProperty;
	t.exports = function (t, e) {
		return n.call(t, e)
	}
}, function (t, e) {
	t.exports = function (t) {
		if (null == t) throw TypeError("Can't call method on " + t);
		return t
	}
}, function (t, e, n) {
	var r = n(5),
		o = n(9),
		i = n(17);
	t.exports = r ? function (t, e, n) {
		return o.f(t, e, i(1, n))
	} : function (t, e, n) {
		return t[e] = n, t
	}
}, function (t, e, n) {
	var r = n(5),
		o = n(27),
		i = n(3),
		a = n(18),
		u = Object.defineProperty;
	e.f = r ? u : function (t, e, n) {
		if (i(t), e = a(e, !0), i(n), o) try {
			return u(t, e, n)
		} catch (t) {}
		if ("get" in n || "set" in n) throw TypeError("Accessors not supported");
		return "value" in n && (t[e] = n.value), t
	}
}, function (t, e, n) {
	var r = n(15),
		o = Math.min;
	t.exports = function (t) {
		return t > 0 ? o(r(t), 9007199254740991) : 0
	}
}, function (t, e, n) {
	var r = n(0),
		o = n(25).f,
		i = n(8),
		a = n(14),
		u = n(19),
		c = n(56),
		l = n(37);
	t.exports = function (t, e) {
		var n, f, s, p, d, v = t.target,
			y = t.global,
			g = t.stat;
		if (n = y ? r : g ? r[v] || u(v, {}) : (r[v] || {}).prototype)
			for (f in e) {
				if (p = e[f], s = t.noTargetGet ? (d = o(n, f)) && d.value : n[f], !l(y ? f : v + (g ? "." : "#") + f, t.forced) && void 0 !== s) {
					if (typeof p == typeof s) continue;
					c(p, s)
				}(t.sham || s && s.sham) && i(p, "sham", !0), a(n, f, p, t)
			}
	}
}, function (t, e, n) {
	var r = n(26),
		o = n(7);
	t.exports = function (t) {
		return r(o(t))
	}
}, function (t, e) {
	var n = {}.toString;
	t.exports = function (t) {
		return n.call(t).slice(8, -1)
	}
}, function (t, e, n) {
	var r = n(0),
		o = n(8),
		i = n(6),
		a = n(19),
		u = n(28),
		c = n(30),
		l = c.get,
		f = c.enforce,
		s = String(String).split("String");
	(t.exports = function (t, e, n, u) {
		var c = !!u && !!u.unsafe,
			l = !!u && !!u.enumerable,
			p = !!u && !!u.noTargetGet;
		"function" == typeof n && ("string" != typeof e || i(n, "name") || o(n, "name", e), f(n).source = s.join("string" == typeof e ? e : "")), t !== r ? (c ? !p && t[e] && (l = !0) : delete t[e], l ? t[e] = n : o(t, e, n)) : l ? t[e] = n : a(e, n)
	})(Function.prototype, "toString", (function () {
		return "function" == typeof this && l(this).source || u(this)
	}))
}, function (t, e) {
	var n = Math.ceil,
		r = Math.floor;
	t.exports = function (t) {
		return isNaN(t = +t) ? 0 : (t > 0 ? r : n)(t)
	}
}, function (t, e, n) {
	"use strict";
	var r, o, i = n(22),
		a = n(45),
		u = RegExp.prototype.exec,
		c = String.prototype.replace,
		l = u,
		f = (r = /a/, o = /b*/g, u.call(r, "a"), u.call(o, "a"), 0 !== r.lastIndex || 0 !== o.lastIndex),
		s = a.UNSUPPORTED_Y || a.BROKEN_CARET,
		p = void 0 !== /()??/.exec("")[1];
	(f || p || s) && (l = function (t) {
		var e, n, r, o, a = this,
			l = s && a.sticky,
			d = i.call(a),
			v = a.source,
			y = 0,
			g = t;
		return l && (-1 === (d = d.replace("y", "")).indexOf("g") && (d += "g"), g = String(t).slice(a.lastIndex), a.lastIndex > 0 && (!a.multiline || a.multiline && "\n" !== t[a.lastIndex - 1]) && (v = "(?: " + v + ")", g = " " + g, y++), n = new RegExp("^(?:" + v + ")", d)), p && (n = new RegExp("^" + v + "$(?!\\s)", d)), f && (e = a.lastIndex), r = u.call(l ? n : a, g), l ? r ? (r.input = r.input.slice(y), r[0] = r[0].slice(y), r.index = a.lastIndex, a.lastIndex += r[0].length) : a.lastIndex = 0 : f && r && (a.lastIndex = a.global ? r.index + r[0].length : e), p && r && r.length > 1 && c.call(r[0], n, (function () {
			for (o = 1; o < arguments.length - 2; o++) void 0 === arguments[o] && (r[o] = void 0)
		})), r
	}), t.exports = l
}, function (t, e) {
	t.exports = function (t, e) {
		return {
			enumerable: !(1 & t),
			configurable: !(2 & t),
			writable: !(4 & t),
			value: e
		}
	}
}, function (t, e, n) {
	var r = n(2);
	t.exports = function (t, e) {
		if (!r(t)) return t;
		var n, o;
		if (e && "function" == typeof (n = t.toString) && !r(o = n.call(t))) return o;
		if ("function" == typeof (n = t.valueOf) && !r(o = n.call(t))) return o;
		if (!e && "function" == typeof (n = t.toString) && !r(o = n.call(t))) return o;
		throw TypeError("Can't convert object to primitive value")
	}
}, function (t, e, n) {
	var r = n(0),
		o = n(8);
	t.exports = function (t, e) {
		try {
			o(r, t, e)
		} catch (n) {
			r[t] = e
		}
		return e
	}
}, function (t, e, n) {
	var r = n(58),
		o = n(0),
		i = function (t) {
			return "function" == typeof t ? t : void 0
		};
	t.exports = function (t, e) {
		return arguments.length < 2 ? i(r[t]) || i(o[t]) : r[t] && r[t][e] || o[t] && o[t][e]
	}
}, function (t, e, n) {
	var r = n(5),
		o = n(1),
		i = n(6),
		a = Object.defineProperty,
		u = {},
		c = function (t) {
			throw t
		};
	t.exports = function (t, e) {
		if (i(u, t)) return u[t];
		e || (e = {});
		var n = [][t],
			l = !!i(e, "ACCESSORS") && e.ACCESSORS,
			f = i(e, 0) ? e[0] : c,
			s = i(e, 1) ? e[1] : void 0;
		return u[t] = !!n && !o((function () {
			if (l && !r) return !0;
			var t = {
				length: -1
			};
			l ? a(t, 1, {
				enumerable: !0,
				get: c
			}) : t[1] = 1, n.call(t, f, s)
		}))
	}
}, function (t, e, n) {
	"use strict";
	var r = n(3);
	t.exports = function () {
		var t = r(this),
			e = "";
		return t.global && (e += "g"), t.ignoreCase && (e += "i"), t.multiline && (e += "m"), t.dotAll && (e += "s"), t.unicode && (e += "u"), t.sticky && (e += "y"), e
	}
}, function (t, e, n) {
	"use strict";
	n(46);
	var r = n(14),
		o = n(1),
		i = n(4),
		a = n(16),
		u = n(8),
		c = i("species"),
		l = !o((function () {
			var t = /./;
			return t.exec = function () {
				var t = [];
				return t.groups = {
					a: "7"
				}, t
			}, "7" !== "".replace(t, "$<a>")
		})),
		f = "$0" === "a".replace(/./, "$0"),
		s = i("replace"),
		p = !!/./ [s] && "" === /./ [s]("a", "$0"),
		d = !o((function () {
			var t = /(?:)/,
				e = t.exec;
			t.exec = function () {
				return e.apply(this, arguments)
			};
			var n = "ab".split(t);
			return 2 !== n.length || "a" !== n[0] || "b" !== n[1]
		}));
	t.exports = function (t, e, n, s) {
		var v = i(t),
			y = !o((function () {
				var e = {};
				return e[v] = function () {
					return 7
				}, 7 != "" [t](e)
			})),
			g = y && !o((function () {
				var e = !1,
					n = /a/;
				return "split" === t && ((n = {}).constructor = {}, n.constructor[c] = function () {
					return n
				}, n.flags = "", n[v] = /./ [v]), n.exec = function () {
					return e = !0, null
				}, n[v](""), !e
			}));
		if (!y || !g || "replace" === t && (!l || !f || p) || "split" === t && !d) {
			var h = /./ [v],
				x = n(v, "" [t], (function (t, e, n, r, o) {
					return e.exec === a ? y && !o ? {
						done: !0,
						value: h.call(e, n, r)
					} : {
						done: !0,
						value: t.call(n, e, r)
					} : {
						done: !1
					}
				}), {
					REPLACE_KEEPS_$0: f,
					REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE: p
				}),
				m = x[0],
				b = x[1];
			r(String.prototype, t, m), r(RegExp.prototype, v, 2 == e ? function (t, e) {
				return b.call(t, this, e)
			} : function (t) {
				return b.call(t, this)
			})
		}
		s && u(RegExp.prototype[v], "sham", !0)
	}
}, function (t, e, n) {
	var r = n(13),
		o = n(16);
	t.exports = function (t, e) {
		var n = t.exec;
		if ("function" == typeof n) {
			var i = n.call(t, e);
			if ("object" != typeof i) throw TypeError("RegExp exec method returned something other than an Object or null");
			return i
		}
		if ("RegExp" !== r(t)) throw TypeError("RegExp#exec called on incompatible receiver");
		return o.call(t, e)
	}
}, function (t, e, n) {
	var r = n(5),
		o = n(51),
		i = n(17),
		a = n(12),
		u = n(18),
		c = n(6),
		l = n(27),
		f = Object.getOwnPropertyDescriptor;
	e.f = r ? f : function (t, e) {
		if (t = a(t), e = u(e, !0), l) try {
			return f(t, e)
		} catch (t) {}
		if (c(t, e)) return i(!o.f.call(t, e), t[e])
	}
}, function (t, e, n) {
	var r = n(1),
		o = n(13),
		i = "".split;
	t.exports = r((function () {
		return !Object("z").propertyIsEnumerable(0)
	})) ? function (t) {
		return "String" == o(t) ? i.call(t, "") : Object(t)
	} : Object
}, function (t, e, n) {
	var r = n(5),
		o = n(1),
		i = n(52);
	t.exports = !r && !o((function () {
		return 7 != Object.defineProperty(i("div"), "a", {
			get: function () {
				return 7
			}
		}).a
	}))
}, function (t, e, n) {
	var r = n(29),
		o = Function.toString;
	"function" != typeof r.inspectSource && (r.inspectSource = function (t) {
		return o.call(t)
	}), t.exports = r.inspectSource
}, function (t, e, n) {
	var r = n(0),
		o = n(19),
		i = r["__core-js_shared__"] || o("__core-js_shared__", {});
	t.exports = i
}, function (t, e, n) {
	var r, o, i, a = n(53),
		u = n(0),
		c = n(2),
		l = n(8),
		f = n(6),
		s = n(54),
		p = n(33),
		d = u.WeakMap;
	if (a) {
		var v = new d,
			y = v.get,
			g = v.has,
			h = v.set;
		r = function (t, e) {
			return h.call(v, t, e), e
		}, o = function (t) {
			return y.call(v, t) || {}
		}, i = function (t) {
			return g.call(v, t)
		}
	} else {
		var x = s("state");
		p[x] = !0, r = function (t, e) {
			return l(t, x, e), e
		}, o = function (t) {
			return f(t, x) ? t[x] : {}
		}, i = function (t) {
			return f(t, x)
		}
	}
	t.exports = {
		set: r,
		get: o,
		has: i,
		enforce: function (t) {
			return i(t) ? o(t) : r(t, {})
		},
		getterFor: function (t) {
			return function (e) {
				var n;
				if (!c(e) || (n = o(e)).type !== t) throw TypeError("Incompatible receiver, " + t + " required");
				return n
			}
		}
	}
}, function (t, e, n) {
	var r = n(55),
		o = n(29);
	(t.exports = function (t, e) {
		return o[t] || (o[t] = void 0 !== e ? e : {})
	})("versions", []).push({
		version: "3.6.4",
		mode: r ? "pure" : "global",
		copyright: "© 2020 Denis Pushkarev (zloirock.ru)"
	})
}, function (t, e) {
	var n = 0,
		r = Math.random();
	t.exports = function (t) {
		return "Symbol(" + String(void 0 === t ? "" : t) + ")_" + (++n + r).toString(36)
	}
}, function (t, e) {
	t.exports = {}
}, function (t, e, n) {
	var r = n(59),
		o = n(60).concat("length", "prototype");
	e.f = Object.getOwnPropertyNames || function (t) {
		return r(t, o)
	}
}, function (t, e, n) {
	var r = n(12),
		o = n(10),
		i = n(36),
		a = function (t) {
			return function (e, n, a) {
				var u, c = r(e),
					l = o(c.length),
					f = i(a, l);
				if (t && n != n) {
					for (; l > f;)
						if ((u = c[f++]) != u) return !0
				} else
					for (; l > f; f++)
						if ((t || f in c) && c[f] === n) return t || f || 0;
				return !t && -1
			}
		};
	t.exports = {
		includes: a(!0),
		indexOf: a(!1)
	}
}, function (t, e, n) {
	var r = n(15),
		o = Math.max,
		i = Math.min;
	t.exports = function (t, e) {
		var n = r(t);
		return n < 0 ? o(n + e, 0) : i(n, e)
	}
}, function (t, e, n) {
	var r = n(1),
		o = /#|\.prototype\./,
		i = function (t, e) {
			var n = u[a(t)];
			return n == l || n != c && ("function" == typeof e ? r(e) : !!e)
		},
		a = i.normalize = function (t) {
			return String(t).replace(o, ".").toLowerCase()
		},
		u = i.data = {},
		c = i.NATIVE = "N",
		l = i.POLYFILL = "P";
	t.exports = i
}, function (t, e, n) {
	"use strict";
	var r = n(62).forEach,
		o = n(43),
		i = n(21),
		a = o("forEach"),
		u = i("forEach");
	t.exports = a && u ? [].forEach : function (t) {
		return r(this, t, arguments.length > 1 ? arguments[1] : void 0)
	}
}, function (t, e) {
	t.exports = function (t) {
		if ("function" != typeof t) throw TypeError(String(t) + " is not a function");
		return t
	}
}, function (t, e, n) {
	var r = n(7);
	t.exports = function (t) {
		return Object(r(t))
	}
}, function (t, e, n) {
	var r = n(13);
	t.exports = Array.isArray || function (t) {
		return "Array" == r(t)
	}
}, function (t, e, n) {
	var r = n(1);
	t.exports = !!Object.getOwnPropertySymbols && !r((function () {
		return !String(Symbol())
	}))
}, function (t, e, n) {
	"use strict";
	var r = n(1);
	t.exports = function (t, e) {
		var n = [][t];
		return !!n && r((function () {
			n.call(null, e || function () {
				throw 1
			}, 1)
		}))
	}
}, function (t, e, n) {
	var r = n(2),
		o = n(13),
		i = n(4)("match");
	t.exports = function (t) {
		var e;
		return r(t) && (void 0 !== (e = t[i]) ? !!e : "RegExp" == o(t))
	}
}, function (t, e, n) {
	"use strict";
	var r = n(1);

	function o(t, e) {
		return RegExp(t, e)
	}
	e.UNSUPPORTED_Y = r((function () {
		var t = o("a", "y");
		return t.lastIndex = 2, null != t.exec("abcd")
	})), e.BROKEN_CARET = r((function () {
		var t = o("^r", "gy");
		return t.lastIndex = 2, null != t.exec("str")
	}))
}, function (t, e, n) {
	"use strict";
	var r = n(11),
		o = n(16);
	r({
		target: "RegExp",
		proto: !0,
		forced: /./.exec !== o
	}, {
		exec: o
	})
}, function (t, e, n) {
	"use strict";
	var r = n(79).charAt;
	t.exports = function (t, e, n) {
		return e + (n ? r(t, e).length : 1)
	}
}, function (t, e, n) {
	"use strict";
	n(49), n(66), n(67), n(72), n(46), n(77), n(78), n(80), n(82), n(84);
	var r = {
		AR: ["es"],
		AT: ["de"],
		AU: ["en"],
		BA: ["bs", "hr", "sr"],
		BE: ["nl", "fr"],
		BG: ["bg"],
		BH: ["ar", "en"],
		CH: ["de", "fr"],
		CA: ["en", "fr"],
		CL: ["es"],
		CO: ["es"],
		CR: ["es"],
		CY: ["el, tr"],
		CZ: ["cs"],
		DE: ["de"],
		DK: ["da"],
		EC: ["es"],
		EE: ["et"],
		ES: ["es", "ca", "ast", "eu", "gl"],
		ET: ["am", "om"],
		FI: ["fi", "sv", "se"],
		FR: ["fr"],
		GR: ["el"],
		HR: ["hr"],
		HU: ["hu"],
		HK: ["hk"],
		IE: ["en", "ga"],
		IL: ["iw", "en"],
		IN: ["hi", "en"],
		IQ: ["ar", "ku"],
		IS: ["is"],
		IT: ["it", "de", "fr"],
		ITG: ["it", "de", "fr"],
		JP: ["ja"],
		KR: ["ko", "en"],
		KW: ["ar", "en"],
		LT: ["lt"],
		LU: ["lb", "fr", "de"],
		LV: ["lv"],
		MC: ["fr"],
		MK: ["mk"],
		MX: ["es"],
		MY: ["en", "ms"],
		NL: ["nl"],
		NO: ["nb", "nn", "no", "se"],
		NZ: ["en", "mi"],
		OM: ["ar", "en"],
		PH: ["fil", "en"],
		PL: ["pl"],
		PT: ["pt"],
		RO: ["ro"],
		RS: ["sr"],
		SA: ["ar", "en"],
		SE: ["sv"],
		SG: ["en", "zh", "ms", "ta"],
		SI: ["sl"],
		SK: ["sk"],
		TH: ["th"],
		TK: ["tkl", "en", "sm"],
		TR: ["tr"],
		TW: ["zh"],
		QA: ["ar", "en"],
		UA: ["uk"],
		AE: ["ar", "en"],
		UK: ["en"],
		UKG: ["en"],
		ZA: ["en", "af"]
	};
	"undefined" != typeof defaultLang && r[u("country").toUpperCase()].unshift(defaultLang);
	var o = {
			MY: {
				symbol: "RM",
				code: "MYR",
				trialValue: "RM 5",
				footerText: {
					en: "ninety nine ringgit"
				}
			},
			AU: {
				symbol: "A$",
				code: "AUD",
				trialValue: "A$1",
				footerText: {
					en: "ninety nine dollars"
				}
			},
			CH: {
				symbol: "CHF",
				code: "CHF",
				trialValue: "1CHF",
				footerText: {
					de: "neunundachtzig CHF",
					fr: "quatre vingt neuf CHF"
				}
			},
			CZ: {
				symbol: "CZK",
				code: "CZK",
				trialValue: "25Kč",
				footerText: {
					cs: "dvacet pět koruna"
				}
			},
			UK: {
				symbol: "£",
				code: "GBP",
				trialValue: "£1",
				footerText: {
					en: "thirty nine british pounds"
				}
			},
			JP: {
				symbol: "¥",
				code: "JPY",
				trialValue: "¥100",
				footerText: {
					ja: ""
				}
			},
			IS: {
				symbol: "KR",
				code: "ISK",
				trialValue: "130KR",
				footerText: {
					is: ""
				}
			},
			NZ: {
				symbol: "$",
				code: "NZD",
				trialValue: "$1",
				footerText: {
					en: "ninety-nine dollar"
				}
			},
			ES: {
				symbol: "€",
				code: "EUR",
				trialValue: "€1",
				footerText: {
					es: "cincuenta y cinco euros"
				}
			},
			IT: {
				symbol: "€",
				code: "EUR",
				trialValue: "€1",
				footerText: {
					it: "cinquantacinque o ottantanove euro"
				}
			},
			ITG: {
				symbol: "€",
				code: "EUR",
				trialValue: "€1",
				footerText: {
					it: "cinquantacinque o ottantanove euro"
				}
			},
			HU: {
				symbol: "Ft",
				code: "HUF",
				trialValue: "324 Ft",
				footerText: {
					hu: "huszonkilenc eurót"
				}
			},
			DE: {
				symbol: "€",
				code: "EURO",
				trialValue: "1€",
				footerText: {
					de: "neunundachtzig Euro"
				}
			},
			SG: {
				symbol: "S$",
				code: "SGD",
				trialValue: "S$1",
				footerText: {
					en: "ninety-nine dollars"
				}
			},
			DK: {
				symbol: "kr",
				code: "DKK",
				trialValue: "7,-",
				footerText: {
					da: "niogfirs euro"
				}
			},
			IE: {
				symbol: "€",
				code: "EUR",
				trialValue: "€1",
				footerText: {
					en: "fifty-five or eighty-nine euro"
				}
			},
			SE: {
				symbol: "kr",
				code: "SEK",
				trialValue: "10,-",
				footerText: {
					sv: "åttio-nio euro"
				}
			},
			TW: {
				symbol: "NT$",
				code: "SEK",
				trialValue: "35 TWD",
				footerText: {
					zh: ""
				}
			},
			LV: {
				symbol: "€",
				code: "EUR",
				trialValue: "€1",
				footerText: {
					lv: "divdesmit deviņiem eiro"
				}
			},
			MK: {
				symbol: "€",
				code: "EUR",
				trialValue: "€1",
				footerText: {
					mk: "дваесет и девет евра"
				}
			},
			HR: {
				symbol: "€",
				code: "EUR",
				trialValue: "€1",
				footerText: {
					hr: "dvadeset devet eura"
				}
			},
			NO: {
				symbol: "€",
				code: "EUR",
				trialValue: "€1",
				footerText: {
					nb: "åtti ni euro"
				}
			},
			PT: {
				symbol: "€",
				code: "EUR",
				trialValue: "€1",
				footerText: {
					pt: "vinte e nove euro"
				}
			},
			GR: {
				symbol: "€",
				code: "EUR",
				trialValue: "€1",
				footerText: {
					el: "πενήντα πέντε ευρώ"
				}
			},
			AT: {
				symbol: "€",
				code: "EURO",
				trialValue: "1€",
				footerText: {
					de: "neunundachtzig Euro"
				}
			},
			SI: {
				symbol: "€",
				code: "EUR",
				trialValue: "€1",
				footerText: {
					sl: "devetindvajset evrov"
				}
			},
			SK: {
				symbol: "€",
				code: "EUR",
				trialValue: "€1",
				footerText: {
					sk: "dvadsať deväť eur"
				}
			},
			UA: {
				symbol: "₴",
				code: "ГРН",
				trialValue: "₴29.99",
				footerText: {
					uk: "сімсот дев'яносто дев'ять гривень"
				}
			},
			TR: {
				symbol: "₺",
				code: "TRY",
				trialValue: "₺6.99",
				footerText: {
					tr: "yüz seksen dokuz lira"
				}
			},
			EE: {
				symbol: "€",
				code: "EUR",
				trialValue: "€1",
				footerText: {
					et: "kakskümmend üheksa eurot"
				}
			},
			RO: {
				symbol: "Leu",
				code: "Leu",
				trialValue: "4.99 Leu",
				footerText: {
					ro: "o sută treizeci și nouă leu"
				}
			},
			KW: {
				symbol: "$",
				code: "USD",
				trialValue: "$1",
				footerText: {
					ar: "$55",
					en: "fifty-five dollars"
				}
			},
			SA: {
				symbol: "$",
				code: "USD",
				trialValue: "$1",
				footerText: {
					ar: "$55",
					en: "fifty-five dollars"
				}
			},
			AE: {
				symbol: "$",
				code: "USD",
				trialValue: "$1",
				footerText: {
					ar: "$55",
					en: "fifty-five dollars"
				}
			},
			OM: {
				symbol: "$",
				code: "USD",
				trialValue: "$1",
				footerText: {
					ar: "$55",
					en: "fifty-five dollars"
				}
			},
			QA: {
				symbol: "$",
				code: "USD",
				trialValue: "$1",
				footerText: {
					ar: "$55",
					en: "fifty-five dollars"
				}
			},
			BH: {
				symbol: "$",
				code: "USD",
				trialValue: "$1",
				footerText: {
					ar: "$55",
					en: "fifty-five dollars"
				}
			},
			BG: {
				symbol: "Лв",
				code: "BGN",
				trialValue: "2 Лв",
				footerText: {
					bg: "двадесет и девет евр"
				}
			},
			HK: {
				symbol: "HK$",
				code: "HKD",
				trialValue: "8 HK$",
				footerText: {
					hk: "430 HK$"
				}
			},
			PL: {
				symbol: "ZŁ",
				code: "PLN",
				trialValue: "5 ZŁ",
				footerText: {
					pl: "dziewięćdziesiąt dziewięć zł"
				}
			},
			NL: {
				symbol: "€",
				code: "EUR",
				trialValue: "€1",
				footerText: {
					nl: "vijfenvijftig euro"
				}
			},
			FR: {
				symbol: "€",
				code: "EUR",
				trialValue: "1€",
				footerText: {
					fr: "cinquante cinq euro"
				}
			},
			BE: {
				symbol: "€",
				code: "EUR",
				trialValue: "€1",
				footerText: {
					nl: "vijfenvijftig euro",
					fr: "cinquante cinq euro"
				}
			},
			IL: {
				symbol: "₪",
				code: "ILS",
				trialValue: "4₪",
				footerText: {
					iw: "מאה ותשעים ותשעה שקלים",
					en: "one hundred and ninety-nine shekels"
				}
			},
			PH: {
				symbol: "₱",
				code: "PHP",
				trialValue: "55 ₱",
				footerText: {
					fil: "isang libo apat na raan siyamnapu't siyam Philippine Peso",
					en: "one thousand four hundred ninety-nine Philippine Peso"
				}
			},
			KR: {
				symbol: "₩",
				code: "KRW",
				trialValue: "1300₩",
				footerText: {
					ko: "49000₩",
					en: "49000₩"
				}
			},
			LU: {
				symbol: "€",
				code: "EUR",
				trialValue: "€1",
				footerText: {
					lb: "nénganechzeg Euro",
					fr: "quatre-vingt-neuf euro",
					de: "neunundachtzig Euro"
				}
			},
			CA: {
				symbol: "CA$",
				code: "CAD",
				trialValue: "1CA$",
				footerText: {
					en: "seventy five Canadian dollars",
					fr: "soixante-quinze dollars canadien"
				}
			},
			UKG: {
				symbol: "£",
				code: "GBP",
				trialValue: "£1",
				footerText: {
					en: "GBP 39"
				}
			}
		},
		i = function () {
			var t = u("country");
			if (t) {
				var e = o[t.toUpperCase()];
				if (e) return e
			}
			return {
				symbol: "€",
				code: "EUR",
				trialValue: "€1",
				footerText: {
					en: "ninety-nine euros"
				}
			}
		}(),
		a = {
			currency: i.symbol,
			"currency-symbol": i.symbol,
			"currency-code": i.code,
			lang: l(),
			"footer-currency": i.footerText[l()],
			"currency-value": i.trialValue
		};

	function u(t) {
		for (var e, n = location.search.substr(1).split("&"), r = 0; r < n.length; r++)
			if (0 === (n[r] + "=").indexOf(t)) {
				e = decodeURIComponent(n[r].slice(t.length + 1));
				break
			}
		return e
	}

	function c(t) {
		return navigator.language.indexOf(t) > -1
	}

	function l() {
		var t = u("country");
		if (t && (t = t.toUpperCase()), r[t]) {
			var e = r[t];
			for (var n in e)
				if (c(e[n])) return e[n];
			return e[0]
		}
		return "it"
	}

	function f(t) {
		var e = t;
		for (var n in a) {
			var r = new RegExp("{" + n + "}", "g");
			e = e.replace(r, a[n])
		}
		return e
	}

	function s(t) {
		for (var e in t) {
			var n = p(e),
				r = n.selector;
			document.querySelectorAll(r).forEach((function (o) {
				var i = f(t[e]);
				r === e ? o.innerHTML = i : o.setAttribute(n.attribute, i)
			}))
		}
		"110" === u("pub") && function () {
			if (!document.getElementById("affiliates")) {
				var t = document.createElement("div");
				t.classList.add("row"), t.style = "margin: 30px 0 10px; width: 100%;", t.innerHTML = '<a id="affiliates" href="https://marketingaffiliateclub.com/" target="_blank" style="width: 100%; text-align: center; color: inherit; font-weight: bold">AFFILIATES</a>';
				var e = document.querySelector(".footer .container, footer");
				e ? e.appendChild(t) : console.error("Missing footer element")
			}
		}()
	}

	function p(t) {
		var e = {};
		return t.indexOf(":") > -1 ? (e.selector = t.split(":")[0], e.attribute = t.split(":")[1], e) : (e.selector = t, e)
	}
	var d, v = function (t) {
			var e = [],
				n = new RegExp("{lang}", "g");
			for (var r in t) {
				var o = t[r].replace(n, a.lang);
				e.push(o)
			}
			return e
		}(function () {
			if ("undefined" != typeof customGlobal) var t = [customGlobal];
			else t = ["json/{lang}.json"];
			if ("undefined" != typeof customTranslationFilesToLoad)
				for (var e in customTranslationFilesToLoad) t.push(customTranslationFilesToLoad[e]);
			return t
		}()),
		y = [];
	v.forEach((function (t, e) {
		var n, r, o;
		n = t, r = function (t) {
			if (y[e] = t, function (t) {
					var e = 0;
					for (var n in t) null != n && e++;
					return e
				}(y) == v.length) {
				for (var n in y) s(y[n]);
				window.onTranslationsRendered && "function" == typeof onTranslationsRendered && window.onTranslationsRendered()
			}
		}, (o = new XMLHttpRequest).onreadystatechange = function () {
			4 == o.readyState && (200 == o.status ? r(JSON.parse(o.responseText)) : onTranslationFileError())
		}, o.open("GET", n, !0), o.send(null)
	})), (d = document.createElement("STYLE")).classList.add("tl_style"), document.querySelector("head").appendChild(d), document.body.classList.add("lang-" + l())
}, function (t, e, n) {
	"use strict";
	var r = n(11),
		o = n(38);
	r({
		target: "Array",
		proto: !0,
		forced: [].forEach != o
	}, {
		forEach: o
	})
}, function (t, e) {
	var n;
	n = function () {
		return this
	}();
	try {
		n = n || new Function("return this")()
	} catch (t) {
		"object" == typeof window && (n = window)
	}
	t.exports = n
}, function (t, e, n) {
	"use strict";
	var r = {}.propertyIsEnumerable,
		o = Object.getOwnPropertyDescriptor,
		i = o && !r.call({
			1: 2
		}, 1);
	e.f = i ? function (t) {
		var e = o(this, t);
		return !!e && e.enumerable
	} : r
}, function (t, e, n) {
	var r = n(0),
		o = n(2),
		i = r.document,
		a = o(i) && o(i.createElement);
	t.exports = function (t) {
		return a ? i.createElement(t) : {}
	}
}, function (t, e, n) {
	var r = n(0),
		o = n(28),
		i = r.WeakMap;
	t.exports = "function" == typeof i && /native code/.test(o(i))
}, function (t, e, n) {
	var r = n(31),
		o = n(32),
		i = r("keys");
	t.exports = function (t) {
		return i[t] || (i[t] = o(t))
	}
}, function (t, e) {
	t.exports = !1
}, function (t, e, n) {
	var r = n(6),
		o = n(57),
		i = n(25),
		a = n(9);
	t.exports = function (t, e) {
		for (var n = o(e), u = a.f, c = i.f, l = 0; l < n.length; l++) {
			var f = n[l];
			r(t, f) || u(t, f, c(e, f))
		}
	}
}, function (t, e, n) {
	var r = n(20),
		o = n(34),
		i = n(61),
		a = n(3);
	t.exports = r("Reflect", "ownKeys") || function (t) {
		var e = o.f(a(t)),
			n = i.f;
		return n ? e.concat(n(t)) : e
	}
}, function (t, e, n) {
	var r = n(0);
	t.exports = r
}, function (t, e, n) {
	var r = n(6),
		o = n(12),
		i = n(35).indexOf,
		a = n(33);
	t.exports = function (t, e) {
		var n, u = o(t),
			c = 0,
			l = [];
		for (n in u) !r(a, n) && r(u, n) && l.push(n);
		for (; e.length > c;) r(u, n = e[c++]) && (~i(l, n) || l.push(n));
		return l
	}
}, function (t, e) {
	t.exports = ["constructor", "hasOwnProperty", "isPrototypeOf", "propertyIsEnumerable", "toLocaleString", "toString", "valueOf"]
}, function (t, e) {
	e.f = Object.getOwnPropertySymbols
}, function (t, e, n) {
	var r = n(63),
		o = n(26),
		i = n(40),
		a = n(10),
		u = n(64),
		c = [].push,
		l = function (t) {
			var e = 1 == t,
				n = 2 == t,
				l = 3 == t,
				f = 4 == t,
				s = 6 == t,
				p = 5 == t || s;
			return function (d, v, y, g) {
				for (var h, x, m = i(d), b = o(m), E = r(v, y, 3), S = a(b.length), T = 0, R = g || u, O = e ? R(d, S) : n ? R(d, 0) : void 0; S > T; T++)
					if ((p || T in b) && (x = E(h = b[T], T, m), t))
						if (e) O[T] = x;
						else if (x) switch (t) {
					case 3:
						return !0;
					case 5:
						return h;
					case 6:
						return T;
					case 2:
						c.call(O, h)
				} else if (f) return !1;
				return s ? -1 : l || f ? f : O
			}
		};
	t.exports = {
		forEach: l(0),
		map: l(1),
		filter: l(2),
		some: l(3),
		every: l(4),
		find: l(5),
		findIndex: l(6)
	}
}, function (t, e, n) {
	var r = n(39);
	t.exports = function (t, e, n) {
		if (r(t), void 0 === e) return t;
		switch (n) {
			case 0:
				return function () {
					return t.call(e)
				};
			case 1:
				return function (n) {
					return t.call(e, n)
				};
			case 2:
				return function (n, r) {
					return t.call(e, n, r)
				};
			case 3:
				return function (n, r, o) {
					return t.call(e, n, r, o)
				}
		}
		return function () {
			return t.apply(e, arguments)
		}
	}
}, function (t, e, n) {
	var r = n(2),
		o = n(41),
		i = n(4)("species");
	t.exports = function (t, e) {
		var n;
		return o(t) && ("function" != typeof (n = t.constructor) || n !== Array && !o(n.prototype) ? r(n) && null === (n = n[i]) && (n = void 0) : n = void 0), new(void 0 === n ? Array : n)(0 === e ? 0 : e)
	}
}, function (t, e, n) {
	var r = n(42);
	t.exports = r && !Symbol.sham && "symbol" == typeof Symbol.iterator
}, function (t, e, n) {
	"use strict";
	var r = n(11),
		o = n(35).indexOf,
		i = n(43),
		a = n(21),
		u = [].indexOf,
		c = !!u && 1 / [1].indexOf(1, -0) < 0,
		l = i("indexOf"),
		f = a("indexOf", {
			ACCESSORS: !0,
			1: 0
		});
	r({
		target: "Array",
		proto: !0,
		forced: c || !l || !f
	}, {
		indexOf: function (t) {
			return c ? u.apply(this, arguments) || 0 : o(this, t, arguments.length > 1 ? arguments[1] : void 0)
		}
	})
}, function (t, e, n) {
	"use strict";
	var r = n(11),
		o = n(2),
		i = n(41),
		a = n(36),
		u = n(10),
		c = n(12),
		l = n(68),
		f = n(4),
		s = n(69),
		p = n(21),
		d = s("slice"),
		v = p("slice", {
			ACCESSORS: !0,
			0: 0,
			1: 2
		}),
		y = f("species"),
		g = [].slice,
		h = Math.max;
	r({
		target: "Array",
		proto: !0,
		forced: !d || !v
	}, {
		slice: function (t, e) {
			var n, r, f, s = c(this),
				p = u(s.length),
				d = a(t, p),
				v = a(void 0 === e ? p : e, p);
			if (i(s) && ("function" != typeof (n = s.constructor) || n !== Array && !i(n.prototype) ? o(n) && null === (n = n[y]) && (n = void 0) : n = void 0, n === Array || void 0 === n)) return g.call(s, d, v);
			for (r = new(void 0 === n ? Array : n)(h(v - d, 0)), f = 0; d < v; d++, f++) d in s && l(r, f, s[d]);
			return r.length = f, r
		}
	})
}, function (t, e, n) {
	"use strict";
	var r = n(18),
		o = n(9),
		i = n(17);
	t.exports = function (t, e, n) {
		var a = r(e);
		a in t ? o.f(t, a, i(0, n)) : t[a] = n
	}
}, function (t, e, n) {
	var r = n(1),
		o = n(4),
		i = n(70),
		a = o("species");
	t.exports = function (t) {
		return i >= 51 || !r((function () {
			var e = [];
			return (e.constructor = {})[a] = function () {
				return {
					foo: 1
				}
			}, 1 !== e[t](Boolean).foo
		}))
	}
}, function (t, e, n) {
	var r, o, i = n(0),
		a = n(71),
		u = i.process,
		c = u && u.versions,
		l = c && c.v8;
	l ? o = (r = l.split("."))[0] + r[1] : a && (!(r = a.match(/Edge\/(\d+)/)) || r[1] >= 74) && (r = a.match(/Chrome\/(\d+)/)) && (o = r[1]), t.exports = o && +o
}, function (t, e, n) {
	var r = n(20);
	t.exports = r("navigator", "userAgent") || ""
}, function (t, e, n) {
	var r = n(5),
		o = n(0),
		i = n(37),
		a = n(73),
		u = n(9).f,
		c = n(34).f,
		l = n(44),
		f = n(22),
		s = n(45),
		p = n(14),
		d = n(1),
		v = n(30).set,
		y = n(76),
		g = n(4)("match"),
		h = o.RegExp,
		x = h.prototype,
		m = /a/g,
		b = /a/g,
		E = new h(m) !== m,
		S = s.UNSUPPORTED_Y;
	if (r && i("RegExp", !E || S || d((function () {
			return b[g] = !1, h(m) != m || h(b) == b || "/a/i" != h(m, "i")
		})))) {
		for (var T = function (t, e) {
				var n, r = this instanceof T,
					o = l(t),
					i = void 0 === e;
				if (!r && o && t.constructor === T && i) return t;
				E ? o && !i && (t = t.source) : t instanceof T && (i && (e = f.call(t)), t = t.source), S && (n = !!e && e.indexOf("y") > -1) && (e = e.replace(/y/g, ""));
				var u = a(E ? new h(t, e) : h(t, e), r ? this : x, T);
				return S && n && v(u, {
					sticky: n
				}), u
			}, R = function (t) {
				t in T || u(T, t, {
					configurable: !0,
					get: function () {
						return h[t]
					},
					set: function (e) {
						h[t] = e
					}
				})
			}, O = c(h), A = 0; O.length > A;) R(O[A++]);
		x.constructor = T, T.prototype = x, p(o, "RegExp", T)
	}
	y("RegExp")
}, function (t, e, n) {
	var r = n(2),
		o = n(74);
	t.exports = function (t, e, n) {
		var i, a;
		return o && "function" == typeof (i = e.constructor) && i !== n && r(a = i.prototype) && a !== n.prototype && o(t, a), t
	}
}, function (t, e, n) {
	var r = n(3),
		o = n(75);
	t.exports = Object.setPrototypeOf || ("__proto__" in {} ? function () {
		var t, e = !1,
			n = {};
		try {
			(t = Object.getOwnPropertyDescriptor(Object.prototype, "__proto__").set).call(n, []), e = n instanceof Array
		} catch (t) {}
		return function (n, i) {
			return r(n), o(i), e ? t.call(n, i) : n.__proto__ = i, n
		}
	}() : void 0)
}, function (t, e, n) {
	var r = n(2);
	t.exports = function (t) {
		if (!r(t) && null !== t) throw TypeError("Can't set " + String(t) + " as a prototype");
		return t
	}
}, function (t, e, n) {
	"use strict";
	var r = n(20),
		o = n(9),
		i = n(4),
		a = n(5),
		u = i("species");
	t.exports = function (t) {
		var e = r(t),
			n = o.f;
		a && e && !e[u] && n(e, u, {
			configurable: !0,
			get: function () {
				return this
			}
		})
	}
}, function (t, e, n) {
	"use strict";
	var r = n(14),
		o = n(3),
		i = n(1),
		a = n(22),
		u = RegExp.prototype,
		c = u.toString,
		l = i((function () {
			return "/a/b" != c.call({
				source: "a",
				flags: "b"
			})
		})),
		f = "toString" != c.name;
	(l || f) && r(RegExp.prototype, "toString", (function () {
		var t = o(this),
			e = String(t.source),
			n = t.flags;
		return "/" + e + "/" + String(void 0 === n && t instanceof RegExp && !("flags" in u) ? a.call(t) : n)
	}), {
		unsafe: !0
	})
}, function (t, e, n) {
	"use strict";
	var r = n(23),
		o = n(3),
		i = n(40),
		a = n(10),
		u = n(15),
		c = n(7),
		l = n(47),
		f = n(24),
		s = Math.max,
		p = Math.min,
		d = Math.floor,
		v = /\$([$&'`]|\d\d?|<[^>]*>)/g,
		y = /\$([$&'`]|\d\d?)/g;
	r("replace", 2, (function (t, e, n, r) {
		var g = r.REGEXP_REPLACE_SUBSTITUTES_UNDEFINED_CAPTURE,
			h = r.REPLACE_KEEPS_$0,
			x = g ? "$" : "$0";
		return [function (n, r) {
			var o = c(this),
				i = null == n ? void 0 : n[t];
			return void 0 !== i ? i.call(n, o, r) : e.call(String(o), n, r)
		}, function (t, r) {
			if (!g && h || "string" == typeof r && -1 === r.indexOf(x)) {
				var i = n(e, t, this, r);
				if (i.done) return i.value
			}
			var c = o(t),
				d = String(this),
				v = "function" == typeof r;
			v || (r = String(r));
			var y = c.global;
			if (y) {
				var b = c.unicode;
				c.lastIndex = 0
			}
			for (var E = [];;) {
				var S = f(c, d);
				if (null === S) break;
				if (E.push(S), !y) break;
				"" === String(S[0]) && (c.lastIndex = l(d, a(c.lastIndex), b))
			}
			for (var T, R = "", O = 0, A = 0; A < E.length; A++) {
				S = E[A];
				for (var L = String(S[0]), w = s(p(u(S.index), d.length), 0), C = [], P = 1; P < S.length; P++) C.push(void 0 === (T = S[P]) ? T : String(T));
				var V = S.groups;
				if (v) {
					var U = [L].concat(C, w, d);
					void 0 !== V && U.push(V);
					var I = String(r.apply(void 0, U))
				} else I = m(L, d, w, C, V, r);
				w >= O && (R += d.slice(O, w) + I, O = w + L.length)
			}
			return R + d.slice(O)
		}];

		function m(t, n, r, o, a, u) {
			var c = r + t.length,
				l = o.length,
				f = y;
			return void 0 !== a && (a = i(a), f = v), e.call(u, f, (function (e, i) {
				var u;
				switch (i.charAt(0)) {
					case "$":
						return "$";
					case "&":
						return t;
					case "`":
						return n.slice(0, r);
					case "'":
						return n.slice(c);
					case "<":
						u = a[i.slice(1, -1)];
						break;
					default:
						var f = +i;
						if (0 === f) return e;
						if (f > l) {
							var s = d(f / 10);
							return 0 === s ? e : s <= l ? void 0 === o[s - 1] ? i.charAt(1) : o[s - 1] + i.charAt(1) : e
						}
						u = o[f - 1]
				}
				return void 0 === u ? "" : u
			}))
		}
	}))
}, function (t, e, n) {
	var r = n(15),
		o = n(7),
		i = function (t) {
			return function (e, n) {
				var i, a, u = String(o(e)),
					c = r(n),
					l = u.length;
				return c < 0 || c >= l ? t ? "" : void 0 : (i = u.charCodeAt(c)) < 55296 || i > 56319 || c + 1 === l || (a = u.charCodeAt(c + 1)) < 56320 || a > 57343 ? t ? u.charAt(c) : i : t ? u.slice(c, c + 2) : a - 56320 + (i - 55296 << 10) + 65536
			}
		};
	t.exports = {
		codeAt: i(!1),
		charAt: i(!0)
	}
}, function (t, e, n) {
	"use strict";
	var r = n(23),
		o = n(3),
		i = n(7),
		a = n(81),
		u = n(24);
	r("search", 1, (function (t, e, n) {
		return [function (e) {
			var n = i(this),
				r = null == e ? void 0 : e[t];
			return void 0 !== r ? r.call(e, n) : new RegExp(e)[t](String(n))
		}, function (t) {
			var r = n(e, t, this);
			if (r.done) return r.value;
			var i = o(t),
				c = String(this),
				l = i.lastIndex;
			a(l, 0) || (i.lastIndex = 0);
			var f = u(i, c);
			return a(i.lastIndex, l) || (i.lastIndex = l), null === f ? -1 : f.index
		}]
	}))
}, function (t, e) {
	t.exports = Object.is || function (t, e) {
		return t === e ? 0 !== t || 1 / t == 1 / e : t != t && e != e
	}
}, function (t, e, n) {
	"use strict";
	var r = n(23),
		o = n(44),
		i = n(3),
		a = n(7),
		u = n(83),
		c = n(47),
		l = n(10),
		f = n(24),
		s = n(16),
		p = n(1),
		d = [].push,
		v = Math.min,
		y = !p((function () {
			return !RegExp(4294967295, "y")
		}));
	r("split", 2, (function (t, e, n) {
		var r;
		return r = "c" == "abbc".split(/(b)*/)[1] || 4 != "test".split(/(?:)/, -1).length || 2 != "ab".split(/(?:ab)*/).length || 4 != ".".split(/(.?)(.?)/).length || ".".split(/()()/).length > 1 || "".split(/.?/).length ? function (t, n) {
			var r = String(a(this)),
				i = void 0 === n ? 4294967295 : n >>> 0;
			if (0 === i) return [];
			if (void 0 === t) return [r];
			if (!o(t)) return e.call(r, t, i);
			for (var u, c, l, f = [], p = (t.ignoreCase ? "i" : "") + (t.multiline ? "m" : "") + (t.unicode ? "u" : "") + (t.sticky ? "y" : ""), v = 0, y = new RegExp(t.source, p + "g");
				(u = s.call(y, r)) && !((c = y.lastIndex) > v && (f.push(r.slice(v, u.index)), u.length > 1 && u.index < r.length && d.apply(f, u.slice(1)), l = u[0].length, v = c, f.length >= i));) y.lastIndex === u.index && y.lastIndex++;
			return v === r.length ? !l && y.test("") || f.push("") : f.push(r.slice(v)), f.length > i ? f.slice(0, i) : f
		} : "0".split(void 0, 0).length ? function (t, n) {
			return void 0 === t && 0 === n ? [] : e.call(this, t, n)
		} : e, [function (e, n) {
			var o = a(this),
				i = null == e ? void 0 : e[t];
			return void 0 !== i ? i.call(e, o, n) : r.call(String(o), e, n)
		}, function (t, o) {
			var a = n(r, t, this, o, r !== e);
			if (a.done) return a.value;
			var s = i(t),
				p = String(this),
				d = u(s, RegExp),
				g = s.unicode,
				h = (s.ignoreCase ? "i" : "") + (s.multiline ? "m" : "") + (s.unicode ? "u" : "") + (y ? "y" : "g"),
				x = new d(y ? s : "^(?:" + s.source + ")", h),
				m = void 0 === o ? 4294967295 : o >>> 0;
			if (0 === m) return [];
			if (0 === p.length) return null === f(x, p) ? [p] : [];
			for (var b = 0, E = 0, S = []; E < p.length;) {
				x.lastIndex = y ? E : 0;
				var T, R = f(x, y ? p : p.slice(E));
				if (null === R || (T = v(l(x.lastIndex + (y ? 0 : E)), p.length)) === b) E = c(p, E, g);
				else {
					if (S.push(p.slice(b, E)), S.length === m) return S;
					for (var O = 1; O <= R.length - 1; O++)
						if (S.push(R[O]), S.length === m) return S;
					E = b = T
				}
			}
			return S.push(p.slice(b)), S
		}]
	}), !y)
}, function (t, e, n) {
	var r = n(3),
		o = n(39),
		i = n(4)("species");
	t.exports = function (t, e) {
		var n, a = r(t).constructor;
		return void 0 === a || null == (n = r(a)[i]) ? e : o(n)
	}
}, function (t, e, n) {
	var r = n(0),
		o = n(85),
		i = n(38),
		a = n(8);
	for (var u in o) {
		var c = r[u],
			l = c && c.prototype;
		if (l && l.forEach !== i) try {
			a(l, "forEach", i)
		} catch (t) {
			l.forEach = i
		}
	}
}, function (t, e) {
	t.exports = {
		CSSRuleList: 0,
		CSSStyleDeclaration: 0,
		CSSValueList: 0,
		ClientRectList: 0,
		DOMRectList: 0,
		DOMStringList: 0,
		DOMTokenList: 1,
		DataTransferItemList: 0,
		FileList: 0,
		HTMLAllCollection: 0,
		HTMLCollection: 0,
		HTMLFormElement: 0,
		HTMLSelectElement: 0,
		MediaList: 0,
		MimeTypeArray: 0,
		NamedNodeMap: 0,
		NodeList: 1,
		PaintRequestList: 0,
		Plugin: 0,
		PluginArray: 0,
		SVGLengthList: 0,
		SVGNumberList: 0,
		SVGPathSegList: 0,
		SVGPointList: 0,
		SVGStringList: 0,
		SVGTransformList: 0,
		SourceBufferList: 0,
		StyleSheetList: 0,
		TextTrackCueList: 0,
		TextTrackList: 0,
		TouchList: 0
	}
}]);
