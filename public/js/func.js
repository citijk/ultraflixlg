function extendShowStep(stepNumber) {
	var elements = document.querySelectorAll('.show-on-step-2');
	if (stepNumber == 2) {
		elements.forEach(el => el.style.display = '');
	} else {
		elements.forEach(el => el.style.display = 'none');
	}
}
